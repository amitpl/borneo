# Document Searching
This project provides the following functionalities.
 * Read the documents from external stores
 * Parse the document to convert it into structured text format
 * Indexing Pipeline to further tokenize the text data and index into ES
 * Searcher to query based on keywords
 
 # Not implemented
 * Doesn't keep track of the documents already collected, and hence those should be ignored in the next run of the collector
 
 # High level Architecture
![Architecture](architecture.jpg)

