package com.borneo;

import com.borneo.api.SearchAPI;
import com.borneo.collector.CollectedDocument;
import com.borneo.collector.CollectionException;
import com.borneo.collector.Collector;
import com.borneo.collector.DocumentMetadata;
import com.borneo.collector.dropbox.DropboxCollector;
import com.borneo.indexer.DocumentIndex;
import com.borneo.indexer.ESService;
import com.borneo.indexer.IndexingException;
import com.borneo.parser.DocumentParser;
import com.borneo.parser.ParseException;
import com.borneo.parser.ParsedDocument;
import com.borneo.parser.TikaDocumentParser;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static spark.Spark.after;

@Slf4j
public class AppMain {

    private static final String DROPBOX_ACCESS_TOKEN = "S__ShEMS8BwAAAAAAAAAARbxaIJGO7xsXkNVk5c8i9l6r1Zqo9uvKOn07d2nh7BT";
    public static final String DROPBOX_APP_NAME = "Borneo-Doc-Search";
    private static final String ES_HOST = "localhost";
    private static final int ES_PORT = 9200;

    public static void main(String[] args) {

        Collector dropboxCollector = new DropboxCollector(DROPBOX_ACCESS_TOKEN, DROPBOX_APP_NAME);
        ESService esService = new ESService(ES_HOST, ES_PORT);
        DocumentParser documentParser = new TikaDocumentParser();
        DocumentIndex documentIndex = new DocumentIndex(esService);

        startDocumentCollection(dropboxCollector, documentParser, documentIndex);

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                dropboxCollector.close();
                documentIndex.close();
            } catch (Exception e) {
                log.error("Exception closing resources", e);
            }
        }));

        after((request, response) -> {
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "GET");
        });

        // This will create the embedded jetty server and wait until the SIGTERM signal is received.
        SearchAPI searchAPI = new SearchAPI(documentIndex);
    }

    private static void startDocumentCollection(Collector dropboxCollector,
                                                DocumentParser documentParser,
                                                DocumentIndex documentIndex) {

        Thread collectionThread = new Thread(() -> {

            while(true) {

                log.info("Starting collection cycle...");

                List<CollectedDocument> collectedDocuments = Collections.emptyList();
                try {

                    List<DocumentMetadata> dms = dropboxCollector.scanForFiles();
                    collectedDocuments = dropboxCollector.downloadBatch(dms);

                } catch (CollectionException e) {
                    log.error("Exception collecting docs", e);
                }

                List<ParsedDocument> parsedDocuments = new ArrayList<>();
                for (CollectedDocument collectedDocument : collectedDocuments) {

                    try {
                        ParsedDocument parsedDocument = documentParser.parse(collectedDocument);
                        parsedDocuments.add(parsedDocument);
                    } catch (ParseException e) {
                        log.error("Exception parsing document: {}", collectedDocument.getDocumentMetadata().getPath(), e);
                    }
                }

                for (ParsedDocument parsedDocument : parsedDocuments) {
                    try {
                        documentIndex.index(parsedDocument);
                    } catch (IndexingException e) {
                        log.error("Exception indexing document: {}", parsedDocument.getDocumentMetadata().getPath(), e);
                    }
                }

                log.info("Successfully Finished collection cycle");

                try {
                    Thread.sleep(30_000L);
                } catch (InterruptedException e) {
                    log.error("Exception in ThreadSleep", e);
                }

            }
        });

        collectionThread.setDaemon(true);
        collectionThread.start();
    }

}
