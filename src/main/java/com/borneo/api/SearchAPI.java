package com.borneo.api;

import com.borneo.indexer.DocumentIndex;
import com.borneo.indexer.SearchingException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import spark.Request;
import spark.Response;

import java.util.List;
import java.util.Map;

import static spark.Spark.get;

@Slf4j
public class SearchAPI {

    private final DocumentIndex documentIndex;
    private static final ObjectMapper objectMapper = new ObjectMapper();

    public SearchAPI(DocumentIndex documentIndex) {
        this.documentIndex = documentIndex;
        get("/search", this::search);
    }

    private Object search(Request request, Response response) {
        String queryText = request.queryParams("query");
        try {
            List<Map<String, Object>> searchResults = documentIndex.search(queryText);
            return objectMapper.writeValueAsString(searchResults);
        } catch (JsonProcessingException | SearchingException e) {
            log.error("Exception in searching for: {}", queryText, e);
        }
        return null;

    }
}
