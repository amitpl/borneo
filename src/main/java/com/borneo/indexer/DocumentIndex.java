package com.borneo.indexer;

import com.borneo.parser.ParsedDocument;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class manages the indexing and searching of the documents downloaded/parsed from external sources
 */
@Slf4j
public class DocumentIndex implements AutoCloseable {

    private static final String INDEX_NAME = "documents";
    private static final String SEARCHABLE_FIELD = "content";

    private final ESService esService;

    public DocumentIndex(ESService esService) {
        this.esService = esService;
    }

    /**
     * Index the document. The index mapping is configured with Synonyms Analyzer to improve the semantic context of the
     * search results.
     *
     * @param parsedDocument The parsed document
     * @throws IndexingException When an error occurs
     */
    public void index(ParsedDocument parsedDocument) throws IndexingException {

        Map<String, Object> doc = new HashMap<>();
        doc.put("content", parsedDocument.getBody());
        doc.put("path", parsedDocument.getDocumentMetadata().getPath());
        doc.put("name", parsedDocument.getDocumentMetadata().getName());
        doc.put("dataSource", parsedDocument.getDataSource().name());
        doc.put("lastModified", parsedDocument.getDocumentMetadata().getLastModified());

        esService.index(parsedDocument.getDocumentMetadata().getPath(), doc, INDEX_NAME);
    }

    /**
     * Searches for the given query.
     * @param query The query string
     * @return The list of matching documents
     * @throws SearchingException When an error occurs
     */
    public List<Map<String, Object>> search(String query) throws SearchingException {
        return esService.search(query, INDEX_NAME, SEARCHABLE_FIELD);
    }

    @Override
    public void close() throws Exception {
        esService.close();
    }
}
