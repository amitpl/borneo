package com.borneo.indexer;

/**
 * This exception wraps the original exception thrown when an ElasticSearch indexing exception occurs
 */
public class IndexingException extends Exception {

    public IndexingException(String message, Throwable cause) {
        super(message, cause);
    }
}
