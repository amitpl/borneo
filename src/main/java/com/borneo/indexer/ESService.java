package com.borneo.indexer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpHost;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * This class generalized the index and search operations of ElasticSearch.
 */
public class ESService implements AutoCloseable {

    private static final ObjectMapper objectMapper = new ObjectMapper();
    private final RestHighLevelClient highLevelClient;

    public ESService(String ip, int port) {
        HttpHost host = new HttpHost(ip, port);
        highLevelClient = new RestHighLevelClient(RestClient.builder(host));
    }

    /**
     * Index one document
     * @param id The id of the doc
     * @param doc The doc source provided in the form of Map<String, Object>
     * @param indexName The index name
     * @throws IndexingException When an error occurs
     */
    public void index(String id, Map<String, Object> doc, String indexName) throws IndexingException {

        try {
            String docJson = objectMapper.writeValueAsString(doc);
            IndexRequest indexRequest = new IndexRequest(indexName).source(docJson, XContentType.JSON);

            // ES will create a new identifier if none is supplied
            if (id != null) {
                indexRequest.id(id);
            }

            try {
                highLevelClient.index(indexRequest, RequestOptions.DEFAULT);
            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (JsonProcessingException e) {
            throw new IndexingException("Error indexing document", e);
        }
    }

    /**
     * Given a query and the index name, this method does a full-text search on the provided field.
     * @param query The query string to search for
     * @param indexName The index against which the search is performed
     * @param field The field against which the search is performed
     * @return The list of matching documents
     * @throws SearchingException When an error occurs
     */
    public List<Map<String, Object>> search(String query, String indexName, String field) throws SearchingException {
        QueryBuilder queryBuilder = QueryBuilders.matchQuery(field, query);
        return search(queryBuilder);
    }

    /**
     * Given a query builder, this method does a full-text search. The query builder encapsulates the complex dsl for various
     * query usecases and hence this method is recommended for complex queries.
     * @param queryBuilder The query builder
     * @return The list of matching documents
     * @throws SearchingException When an error occurs
     */
    public List<Map<String, Object>> search(QueryBuilder queryBuilder) throws SearchingException {
        SearchSourceBuilder searchSourceBuilder = SearchSourceBuilder.searchSource().query(queryBuilder);
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.source(searchSourceBuilder);
        try {
            SearchResponse searchResponse = highLevelClient.search(searchRequest, RequestOptions.DEFAULT);
            List<Map<String, Object>> results = new ArrayList<>();
            for (SearchHit hit : searchResponse.getHits().getHits()) {
                results.add(hit.getSourceAsMap());
            }
            return results;
        } catch (IOException e) {
            throw new SearchingException("Error in searching", e);
        }
    }

    @Override
    public void close() throws Exception {
        if (highLevelClient != null) {
            highLevelClient.close();
        }
    }
}
