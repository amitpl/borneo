package com.borneo.indexer;

/**
 * This exception wraps the original exception thrown when an ElasticSearch searching exception occurs
 */
public class SearchingException extends Exception {

    public SearchingException(String message, Throwable cause) {
        super(message, cause);
    }
}
