package com.borneo.parser;

import com.borneo.collector.CollectedDocument;
import com.borneo.collector.ContentType;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.tika.Tika;
import org.apache.tika.metadata.Metadata;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Reader;
import java.util.Objects;

/**
 * Apache TIKA based implementation of Document Parser.
 */
@Slf4j
public class TikaDocumentParser implements DocumentParser {

    public static final String CONTENT_TYPE_KEY = "Content-Type";
    private final Tika tika = new Tika();

    /**
     * Parse the collected document. This method auto-detects the content-type of the collected document and uses the
     * correct parser accordingly. For now, the supported documents are PDF and DOCX. The parsed content is represented
     * as a String.
     *
     * @param collectedDocument The collected document
     * @return The parsed document
     * @throws ParseException When an error occurs
     */
    @Override
    public ParsedDocument parse(CollectedDocument collectedDocument) throws ParseException {
        Objects.requireNonNull(collectedDocument, "Missing collected document");
        Objects.requireNonNull(collectedDocument.getContent(), "Missing document content");
        Objects.requireNonNull(collectedDocument.getDocumentMetadata(), "Missing document metadata");

        Metadata metadata = new Metadata();

        try (Reader reader = tika.parse(new ByteArrayInputStream(collectedDocument.getContent()), metadata)) {

            String body = IOUtils.toString(reader);
            String contentTypeStr = metadata.get(CONTENT_TYPE_KEY);

            log.info("[{}}: Content Type: {}", collectedDocument.getDocumentMetadata().getName(), contentTypeStr);

            ContentType contentType = ContentType.fromString(contentTypeStr);

            isContentTypeSupported(contentType);

            return ParsedDocument.builder()
                    .body(body)
                    .contentType(contentType)
                    .documentMetadata(collectedDocument.getDocumentMetadata())
                    .dataSource(collectedDocument.getSource())
                    .build();

        } catch (IOException e) {
            throw new ParseException(String.format("Error Parsing document: %s", collectedDocument.getDocumentMetadata().getName()), e);
        }
    }
}
