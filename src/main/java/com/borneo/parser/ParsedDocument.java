package com.borneo.parser;

import com.borneo.collector.ContentType;
import com.borneo.collector.DataSource;
import com.borneo.collector.DocumentMetadata;
import lombok.Builder;
import lombok.Getter;

/**
 * POJO to hold the parsed doc info particularly the parsed text content, content type of the document.
 */
@Builder
@Getter
public class ParsedDocument {

    private final String body;
    private final ContentType contentType;
    private final DocumentMetadata documentMetadata;
    private final DataSource dataSource;

}
