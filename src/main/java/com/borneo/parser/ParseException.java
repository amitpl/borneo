package com.borneo.parser;

/**
 * This exception wraps the original exception thrown during parsing.
 */
public class ParseException extends Exception {

    public ParseException(String message, Throwable cause) {
        super(message, cause);
    }

    public ParseException(String message) {
        super(message);
    }
}
