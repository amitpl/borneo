package com.borneo.parser;

import com.borneo.collector.CollectedDocument;
import com.borneo.collector.ContentType;

/**
 * This interface provides the method(s) for parsing the document.
 */
public interface DocumentParser {

    /**
     * Parse the collected document. This method auto-detects the content-type of the collected document and uses the
     * correct parser accordingly. For now, the supported documents are PDF and DOCX. The parsed content is represented
     * as a String.
     * @param collectedDocument The collected document
     * @return The parsed document
     * @throws ParseException When an error occurs
     */
    ParsedDocument parse(CollectedDocument collectedDocument) throws ParseException;

    /**
     * Allows the supported content type {@link ContentType#PDF} and {@link ContentType#DOCX}
     * @param contentType The detected content type of the document
     * @throws ParseException Thrown if the content type isn't supported
     */
    default void isContentTypeSupported(ContentType contentType) throws ParseException {
        if (contentType != ContentType.PDF && contentType != ContentType.DOCX) {
            throw new ParseException(String.format("Unsupported Content Type: %s", contentType));
        }
    }
}
