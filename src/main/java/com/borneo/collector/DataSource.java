package com.borneo.collector;

/**
 * Enum to represent the data-sources supported by the application.
 */
public enum DataSource {
    DROPBOX,
    S3,
    ;
}
