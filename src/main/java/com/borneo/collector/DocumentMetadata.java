package com.borneo.collector;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.util.Date;

/**
 * POJO to hold the doc metadata such as name, path, its modified time, the hash of the content etc.
 */
@Builder
@Getter
@EqualsAndHashCode
@ToString
public class DocumentMetadata {
    private final String name;
    private final String path;
    private final Date lastModified;
    private final String contentHash;
    private final String contentType;
    private final long size;
}