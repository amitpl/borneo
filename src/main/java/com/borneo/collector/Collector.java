package com.borneo.collector;

import java.util.ArrayList;
import java.util.List;

/**
 * This interface provides the methods to download the documents from external stores and store them as internal POJO
 */
public interface Collector extends AutoCloseable {

    int DEFAULT_BATCH_SIZE = 20;

    /**
     * Find all the files present in the external store. This method gives the metadata associated with all the files.
     * @return The metadata of all the files/documents present in the external store
     * @throws CollectionException When an error occurs
     */
    List<DocumentMetadata> scanForFiles() throws CollectionException;

    /**
     * Downloads the file given the metadata. This is an synchronous operation and returns the downloaded content as
     * a byte array. It further updates the documentMetadata to reflect the current state of the downloaded file.
     * @param documentMetadata The document metadata
     * @return The collected document
     * @throws CollectionException When an error occurs
     */
    CollectedDocument download(DocumentMetadata documentMetadata) throws CollectionException;

    /**
     * Provides the ability to download a batch of files at once. This is especially useful for stores that provide optimized
     * batch API to download files. The default implementation unfolds the batch and downloads one file at a time.
     * @param metadataList The list of document metadata
     * @return The list of collected documents
     * @throws CollectionException When an error occurs. Note: The complete batch will fail.
     */
    default List<CollectedDocument> downloadBatch(List<DocumentMetadata> metadataList) throws CollectionException{
        List<CollectedDocument> documents = new ArrayList<>();
        for (DocumentMetadata documentMetadata : metadataList) {
            documents.add(download(documentMetadata));
        }
        return documents;
    }

    /**
     * The recommended batch size for use with {@link Collector#downloadBatch}. Individual implementation must provide
     * the optimum batch size for best performance.
     * @return The batch size.
     */
    default int getBatchSize() {
        return DEFAULT_BATCH_SIZE;
    }
}
