package com.borneo.collector;

/**
 * Enum for the Content Type of the document.
 */
public enum ContentType {
    PDF,
    DOCX,
    UNKNOWN,
    ;

    /**
     * Find the content type from string representation
     * @param contentType The string content type
     * @return The enum content type
     */
    public static ContentType fromString(String contentType) {
        if (contentType == null) {
            return null;
        }

        switch (contentType.toLowerCase()) {
            case "application/pdf":
                return PDF;
            case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
                return DOCX;
            default:
                return UNKNOWN;
        }
    }
}
