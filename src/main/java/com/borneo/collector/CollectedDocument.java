package com.borneo.collector;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

/**
 * POJO to hold the details of the collected document along with its content in the form of Byte Array
 */
@AllArgsConstructor
@Builder
@Getter
public class CollectedDocument {

    private final DataSource source;
    private final String customerId;
    private final byte[] content;
    private final DocumentMetadata documentMetadata;

}
