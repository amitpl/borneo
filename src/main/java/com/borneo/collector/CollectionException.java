package com.borneo.collector;

/**
 * This exception wraps the original exception thrown in case of collection error
 */
public class CollectionException extends Exception {

    public CollectionException(String message, Throwable cause) {
        super(message, cause);
    }
}
