package com.borneo.collector.dropbox;

import com.borneo.collector.CollectedDocument;
import com.borneo.collector.CollectionException;
import com.borneo.collector.Collector;
import com.borneo.collector.ContentType;
import com.borneo.collector.DataSource;
import com.borneo.collector.DocumentMetadata;
import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.FileMetadata;
import com.dropbox.core.v2.files.ListFolderResult;
import com.dropbox.core.v2.files.Metadata;
import lombok.extern.slf4j.Slf4j;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * This class adds the support for DROPBOX as an external store. It provides methods to scan
 */
@Slf4j
public class DropboxCollector implements Collector {

    public static final int MAX_RETRIES = 3;

    private final String apiAccessToken;
    private final String appName;
    private final DbxClientV2 client;

    public DropboxCollector(String apiAccessToken, String appName) {
        this.apiAccessToken = apiAccessToken;
        this.appName = appName;

        DbxRequestConfig dbxRequestConfig = DbxRequestConfig.newBuilder(appName)
                .withAutoRetryEnabled(MAX_RETRIES)
                .build();

        client = new DbxClientV2(dbxRequestConfig, apiAccessToken);
    }

    /**
     * Find all the files present in the external store. This method gives the metadata associated with all the files.
     *
     * @return The metadata of all the files/documents present in the external store
     * @throws CollectionException When an error occurs
     */
    @Override
    public List<DocumentMetadata> scanForFiles() throws CollectionException {
        try {

            List<DocumentMetadata> scannedFiles = new ArrayList<>();

            ListFolderResult result = client.files()
                    .listFolderBuilder("")
                    .withRecursive(true)
                    .start();

            log.info("[{}]: ListFolder-Result: {}", appName, result);

            while (true) {

                for (Metadata metadata : result.getEntries()) {

                    if (metadata instanceof FileMetadata) {
                        DocumentMetadata documentMetadata = buildDocMetadata((FileMetadata) metadata);
                        scannedFiles.add(documentMetadata);
                    }
                }

                if (!result.getHasMore()) {
                    break;
                }

                result = client.files().listFolderContinue(result.getCursor());
            }
            return scannedFiles;

        } catch (DbxException e) {
            throw new CollectionException(String.format("Error scanning files for app: %s", appName), e);
        }
    }

    /**
     * Downloads the file given the metadata. This is an synchronous operation and returns the downloaded content as
     * a byte array. It further updates the documentMetadata to reflect the current state of the downloaded file.
     *
     * @param documentMetadata The document metadata
     * @return The collected document
     * @throws CollectionException When an error occurs
     */
    @Override
    public CollectedDocument download(DocumentMetadata documentMetadata) throws CollectionException {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            FileMetadata metadata = client.files().download(documentMetadata.getPath()).download(baos);

            // Since file-metadata can change between the operations of scanning for files and downloading the file,
            // we will update file-metadata with the latest info which was received at the time of downloading
            DocumentMetadata updatedDm = buildDocMetadata(metadata);

            return CollectedDocument.builder()
                    .content(baos.toByteArray())
                    .documentMetadata(updatedDm)
                    .source(DataSource.DROPBOX)
                    .build();

        } catch (DbxException | IOException e) {
            throw new CollectionException(String.format("Error downloading %s for app: %s", documentMetadata.getPath(), appName), e);
        }

    }

    private DocumentMetadata buildDocMetadata(FileMetadata fileMetadata) {
        return DocumentMetadata.builder()
                .name(fileMetadata.getName())
                .path(fileMetadata.getPathLower())
                .contentHash(fileMetadata.getContentHash())
                .contentType(ContentType.UNKNOWN.name())
                .lastModified(fileMetadata.getClientModified())
                .size(fileMetadata.getSize())
                .build();
    }

    @Override
    public void close() throws Exception {
        // DbxClientV2 doesn't have a close method
    }
}
